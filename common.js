Matches = new Mongo.Collection("matches");

Matches.attachSchema(new SimpleSchema({
  topic: {
    type: String,
    optional: false,
  },
}));

Rounds = new Mongo.Collection("rounds");

Rounds.attachSchema(new SimpleSchema({
  
  matches:{
    type: [String],
    optional: true,
  },
  'matches.$': {
    autoform: {
      afFieldInput: {
        type: 'autocomplete-input',
        position: "top",
        placeholder: 'Topic',
        settings: {
          position: "top",
          limit: 5,
          rules: [
            {
              collection: Matches,
              field: "topic",
              template: Meteor.isClient && Template.autocomplete_display
            },
          ]
        }
      }
    }
  }
}));
